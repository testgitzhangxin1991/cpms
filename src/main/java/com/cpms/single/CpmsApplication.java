package com.cpms.single;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author gulang
 * @Description:
 * @time: 2021/11/24 21:38
 */
@SpringBootApplication
@Slf4j
public class CpmsApplication {
    public static void main(String[] args)
    {
        try {
            SpringApplication.run(CpmsApplication.class, args);
            log.info("[Cpms-admin-interface-App] startup success !!!");
            System.err.println("(♥◠‿◠)ﾉﾞ  Cpms服务启动成功   ლ(´ڡ`ლ)ﾞ");
        }catch(Throwable e) {
            e.printStackTrace();
        }
    }
}
